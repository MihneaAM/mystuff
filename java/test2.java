class test2 {
	public static void main (String args[]) {
		String str1 = new String("string1");
		String str2 = "string2";
		String str3 = new String(str2);

		System.out.print(str1 + "\n" + str2 + "\n" + str3 + "\n");

		System.out.println(str3.equals(str2));
	}
}
