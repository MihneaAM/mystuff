class StaticBlock {
	static double rootOf2;
	static double rootOf3;
	int x;

	static {
		System.out.println("Inside static block");
		rootOf2 = Math.sqrt(2.0);
		rootOf3 = Math.sqrt(3.0);
		//x = 2;
	}

	StaticBlock(String msg) {
		System.out.println(msg);
	}

	void show() {
		System.out.println("StaticBlock");
	}
}

class StaticBlock2 extends StaticBlock {

	StaticBlock2() {
		super("blabla");
	}

	void show() {
		System.out.println("StaticBlock2");
	}
}

class pacman {

	static void vaTest(int ... v) {
		System.out.println("Number of args: " + v.length);
		System.out.println("Contents: ");

		for(int i = 0; i < v.length; i++) {
			System.out.println("arg " + i + ": " + v[i]);
		}
		System.out.println();
	}

	static void test() {
	}
	
	public static void main(String args[]) {
	//	StaticBlock ob = new StaticBlock("Inside constructor");
		vaTest(10);
		vaTest(1, 2, 3);
		vaTest();
		test();
		StaticBlock2 ob1 = new StaticBlock2();
		ob1.show();
	}
}