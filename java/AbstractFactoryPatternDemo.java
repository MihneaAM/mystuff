interface Shape {

	public void draw();
}

class Circle implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Circle::draw() method");
	}
}

class Square implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Square::draw() method");
	}
}

class Rectangle implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Rectangle::draw() method");
	}
}

interface Color {

	public void fill();
}

class Red implements Color {

	@Override
	public void fill() {
		System.out.println("Inside Red::fill() method");
	}
}

class Green implements Color {

	@Override
	public void fill() {
		System.out.println("Inside Green::fill() method");
	}
}

class Blue implements Color {

	@Override
	public void fill() {
		System.out.println("Inside Blue::fill() method");
	}
}

abstract class AbstractFactory {

	abstract Shape getShape(String shape);
	abstract Color getColor(String color);
}

class ShapeFactory extends AbstractFactory {

	public Shape getShape(String shapeType) {
		if(shapeType == null) {
			return null;
		}

		if(shapeType.equalsIgnoreCase("CIRCLE")) {
			return new Circle();
		} else if(shapeType.equalsIgnoreCase("SQUARE")) {
			return new Square();
		} else if(shapeType.equalsIgnoreCase("RECTANGLE")) {
			return new Rectangle();
		}

		return null;
	}

	public Color getColor(String color) {
		return null;
	}
}

class ColorFactory extends AbstractFactory {

	public Shape getShape(String shapeType) {
		return null;
	}

	public Color getColor(String color) {
		if(color == null) {
			return null;
		}

		if(color.equalsIgnoreCase("RED")) {
			return new Red();
		} else if(color.equalsIgnoreCase("GREEN")) {
			return new Green();
		} else if(color.equalsIgnoreCase("BLUE")) {
			return new Blue();
		}

		return null;
	}
}

class FactoryProducer {

	public static AbstractFactory getFactory(String choice) {
		if(choice.equalsIgnoreCase("SHAPE")) {
			return new ShapeFactory();
		} else if(choice.equalsIgnoreCase("COLOR")) {
			return new ColorFactory();
		}

		return null;
	}
}

public class AbstractFactoryPatternDemo {

	public static void main(String args[]) {

		AbstractFactory shapeFactory = FactoryProducer.getFactory("SHAPE");

		Shape shape1 = shapeFactory.getShape("CIRCLE");
		shape1.draw();

		Shape shape2 = shapeFactory.getShape("SQUARE");
		shape2.draw();

		Shape shape3 = shapeFactory.getShape("RECTANGLE");
		shape3.draw();


		AbstractFactory colorFactory = FactoryProducer.getFactory("COLOR");

		Color color1 = colorFactory.getColor("red");
		color1.fill();

		Color color2 = colorFactory.getColor("green");
		color2.fill();

		Color color3 = colorFactory.getColor("blue");
		color3.fill();
	}
}