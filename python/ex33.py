i = 0
numbers = []

def funct(x, y):
	global i
	while i < x:
		print "At the top i is %d" %i
		numbers.append(i)

		i += y
		print "Numbers now: ", numbers

		print "At the bottom i is %d" %i

funct(3, 1)

print "The numbers: "

for num in numbers:
	print num
